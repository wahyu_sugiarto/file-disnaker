<?php
$url = "http://localhost/Api_Disnaker/WebService/getCity";
$client = curl_init($url);
curl_setopt($client, CURLOPT_RETURNTRANSFER, 1);
$response = curl_exec($client);
$result = json_decode($response, true);
?>

<!DOCTYPE html>
<html lang="zxx">

<head>
    <title> Lembaga Pelatihan Kerja</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script>
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!-- Custom Theme files -->
    <script src="assets/jquery-3.3.1.js"></script>
    <link rel="stylesheet" href="assets/popup/jquery.fancybox.css" />
    <link rel="stylesheet" href="assets/gaya.css" />
    <script src="assets/popup/jquery.fancybox.js"></script>
    <link href="css/bootstrap.css" type="text/css" rel="stylesheet" media="all">
    <link href="css/style.css" type="text/css" rel="stylesheet" media="all">
    <!-- font-awesome icons -->
    <link href="css/fontawesome-all.min.css" rel="stylesheet">
	<!-- //Custom Theme files -->
    <!-- online-fonts -->
    <link href="//fonts.googleapis.com/css?family=Ubuntu:300,300i,400,400i,500,500i,700,700i" rel="stylesheet">
    <!-- //online-fonts -->
</head>

<body>
    <!-- banner -->
    <div class="banner">
        <!-- header -->
    <header>	
	<nav class="mnu navbar-light">
            <div class="logo" id="logo">
                <h1><a href="index.html">Balai Latihan Kerja Jawa Timur</a></h1>
            </div>
				<label for="drop" class="toggle"><span class="fa fa-bars"></span></label>
                <input type="checkbox" id="drop">
                    <ul class="menu">
                        <li class="mr-lg-4 mr-3 active"><a href="index.php">Home</a></li>
						<li class="mr-lg-4 mr-3"><a href="about.php">About</a></li>
                        <li class="mr-lg-4 mr-3"><a href="services.php">Program Pelatihan</a></li>
						 <li class="mr-lg-4 mr-3"><a href="services.php">Contact</a></li>
                            <!-- First Tier Drop Down -->
                            
                  
    </nav>
</header>
<!-- //header -->
        <div class="container">
            <!-- banner-text -->
            <div class="banner-text">
                <div class="slider-info">
                    <h3>Ikuti pelatihan dan tingkatkan keahlian mu sekarang juga!</h3>
				</div>
            </div>
			<div class="banner-top pb-5">
                <div class="row slider-bottom">
                    <div class="col-md-3 slider-bottom-lft">
						<h4>Untuk Info lebih lanjut</h4>
						<p class="text-white mt-2">Hubungi BLK kota/Kabupaten di Jawa Timur</p>
					</div>
					 <div class="col-md-9 n-right-w3ls">
						<div class="row">
							<div class="col-md-4 form-group news-rt">
								<input class="form-control" type="text" name="Name" placeholder="Nama" required="">
							</div>
							<div class="col-md-4 form-group news-lt">
								<input class="form-control" type="email" name="Email" placeholder=" Kota" required="">
							</div>
							<div class="col-md-4 form-group news-last">
								<div class="sbm-it">
									<div class="form-group">
										<input class="form-control submit text-uppercase" type="submit" value="Subscribe">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
            </div>
        </div>
    </div>
	 <!-- //banner-text -->
	<section class="about-w3ls py-5">
		<div class="container pt-xl-5 pb-lg-3">
			<div class="row">
				<div class="col-lg-7">
					<img src="images/2.jpg" alt="" class="img-section4 img-fluid">
				</div>
				<div class="col-lg-5 section-4">
					<div class="agil_mor">
						<h2 class="heading-agileinfo">Apa <span> itu BLK ?</span></h2>
						<p class="vam">Balai Latihan Kerja atau sering disebut dengan singkatan BLK adalah prasarana dan sarana tempat pelatihan untuk mendapatkan keterampilan atau yang ingin mendalami keahlian dibidangnya masing-masing.

  </p>
						
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="about-w3ls py-5">
		<div class="container pt-xl-5 pb-lg-3">
			<div class="row">
				<div class="col-lg-5 section-5">
					<div class="agil_mor">
						<h3 class="heading-agileinfo">Kami <span> Menawarkan</span></h3>
						<p class="vam">BLK adalah membuka beberapa bidang kejuruan seperti, Kejuruan Teknik Sepeda Motor, Kejuruan Teknisi Komputer, Kejuruan Operator Komputer, Kejuruan Tata Busana, Kejuruan Teknik Pendingin, Kejuruan Tata Graha, Kejuruan Tata Boga dan lain sebagainya. Bahkan keberadaan BLK juga bisa memfasilitasi untuk keahlian dalam bidang bahasa asing seperti, Bahasa Inggris, Bahasa Jepang dan Bahasa Korea. </p>
						
					</div>
				</div>
				<div class="col-lg-7">
					<img src="images/1.jpg" alt="" class="img-section4 img-fluid">
				</div>
				
			</div>
		</div>
	</section>
 <!-- stats -->
    <section class="agile_stats py-sm-5">
        <div class="container">
            <div class="py-lg-5 w3-abbottom">
                <div class="row py-5">
                    <div class="counter col-lg-3 col-6">
                        <span class="fa fa-smile-o"></span>
                        <h4 class="timer mt-2">5,100</h4>
                        <p class="count-text text-capitalize">JUMLAH PELATIHAN</p>
                    </div>

                    <div class="counter col-lg-3 col-6">
                        <span class="fa fa-fighter-jet"></span>
                         <h4 class="timer mt-2">971</h4>
                        <p class="count-text text-capitalize">CABANG BLK</p>
                    </div>
                    <div class="counter col-lg-3 col-6 mt-lg-0 mt-4">
                        <span class="fa fa-users"></span>
                        <h4 class="timer mt-2">21</h4>
                        <p class="count-text text-capitalize">PENGAJAR</p>
                    </div>
                    <div class="counter col-lg-3 col-6 mt-lg-0 mt-4">
                       <span class="fa fa-comments"></span>
                         <h4 class="timer mt-2">27</h4>
                        <p class="count-text text-capitalize">ALUMNI BLK </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- //stats -->
	 <!-- services -->
    <div class="more-services py-lg-5">
		<div class="container py-5">
            <div class="title-section pb-sm-5 pb-3">
               <h3 class="heading-agileinfo text-center pb-4">Kantor  <span> BLK</span></h3>
            </div>
            <div class="row grid">
            	<?php 
            	foreach (array_combine($result['data_city'], $result['alamat']) as $value => $key) { 


              
            	$dataExp = explode(' ', $value);
            	?>

            	  <div class="col-lg-3 col-6 more-services-1">
                    <figure class="effect-1">
                        <a class="clear fancybox fancybox.iframe" href="detail_blk.php?name=<?php echo strtolower($dataExp[2]) ?>"><center><img src="images/jatimlogo.png" alt="img" class="img-fluid" width="150px" /></center>
                        <h4><?php echo ucfirst(strtolower($dataExp[2])) ?></h4></a>
                        <p><?php echo $key ?> </p>
                      
					</figure>
				 </div>
				  <?php } ?>
            </div>
			
        </div>
    </div>

    <!-- //services -->
	<!-- testimonials -->
	<div class="testimonials py-lg-5">
		<div class="container py-5">
			 <div class="title-section pb-sm-5 pb-3">
               <h3 class="heading-agileinfo text-center pb-4">Alumni<span> BLK</span></h3>
            </div>
			<div class="mis-stage">
				<!-- The element to select and apply miSlider to - the class is optional -->
				<div class="row mis-slider">
					<!-- The slider element - the class is optional -->
					<div class="col-lg-2 col-4 mis-slide mb-4">
						<img src="images/te1.jpg" alt=" " class="img-fluid" />
						<h6>Carl Lii</h6>
					</div>
					<div class="col-lg-2 col-4 mis-slide mb-4">
						<img src="images/te2.jpg" alt=" " class="img-fluid" />
						<h6>Michael Paul</h6>
					</div>
					<div class="col-lg-2 col-4 mis-slide mb-4">
						<img src="images/te3.jpg" alt=" " class="img-fluid" />
						<h6>Henry Doe</h6>
					</div>
					<div class="col-lg-2 col-4 mis-slide mb-4">
						<img src="images/te4.jpg" alt=" " class="img-fluid" />
						<h6>Laura James</h6>
					</div>
					<div class="col-lg-2 col-4 mis-slide mb-4">
						<img src="images/te5.jpg" alt=" " class="img-fluid" />
						<h6>Thomas Carl</h6>
					</div>
					<div class="col-lg-2 col-4 mis-slide mb-4">
						<img src="images/te6.jpg" alt=" " class="img-fluid" />
						<h6>Rosy Crisp</h6>
					</div>
					
				</div>
			</div>
		</div>
	</div>
	<!-- //testimonials -->
<!-- video and events -->
	<div class="video-choose-agile py-lg-5">
		<div class="container py-5">
			<div class="title-section pb-sm-5 pb-3">
				<h3 class="heading-agileinfo text-center pb-4">Info <span>Terbaru</span></h3>
			</div>
			<div class="row">
				<div class="col-lg-5 events">
					<div class="events-w3ls">
						<div class="d-flex">
							<div class="col-sm-2 col-3 events-up p-2 text-center">
								<h5 class="font-weight-bold">18
									<span class="border-top font-weight-light pt-2 mt-2">Feb</span>
								</h5>
							</div>
							<div class="col-sm-10 col-9 events-right">
								<h4 class="Cur">Curabitur mattis orci </h4>
								<ul class="list-unstyled">
									<li class="my-2">
										<span class="fa fa-clock-o mr-2"></span>5.00 pm - 7.30 pm</li>
									<li>
										<span class="fa fa-map-marker mr-2"></span>25 Newyork City.</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="d-flex my-4">
						<div class="col-sm-2 col-3 events-up p-2 text-center">
							<h5 class="font-weight-bold">22
								<span class="border-top font-weight-light pt-2 mt-2">Feb</span>
							</h5>
						</div>
						<div class="col-sm-10 col-9 events-right">
							<h4 class="Cur">Curabitur mattis orci </h4>
							<ul class="list-unstyled">
								<li class="my-2">
									<span class="fa fa-clock-o mr-2"></span>5.00 pm - 7.30 pm</li>
								<li>
									<span class="fa fa-map-marker mr-2"></span>25 Newyork City.</li>
							</ul>
						</div>
					</div>
					<div class="d-flex">
						<div class="col-sm-2 col-3 events-up p-2 text-center">
							<h5 class="font-weight-bold">25
								<span class="border-top font-weight-light pt-2 mt-2">Feb</span>
							</h5>
						</div>
						<div class="col-sm-10 col-9 events-right">
							<h4 class="Cur">Curabitur mattis orci </h4>
							<ul class="list-unstyled">
								<li class="my-2">
									<span class="fa fa-clock-o mr-2"></span>5.00 pm - 7.30 pm</li>
								<li>
									<span class="fa fa-map-marker mr-2"></span>25 Newyork City.</li>
							</ul>
						</div>
					</div>
					<div class="d-flex mt-4">
						<div class="col-sm-2 col-3 events-up p-2 text-center">
							<h5 class="font-weight-bold">28
								<span class="border-top font-weight-light pt-2 mt-2">Feb</span>
							</h5>
						</div>
						<div class="col-sm-10 col-9 events-right">
							<h4 class="Cur">Curabitur mattis orci </h4>
							<ul class="list-unstyled">
								<li class="my-2">
									<span class="fa fa-clock-o mr-2"></span>5.00 pm - 7.30 pm</li>
								<li>
									<span class="fa fa-map-marker mr-2"></span>25 Newyork City.</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-lg-7 video">
					<img src="images/g1.jpg" class="img-fluid" alt="" />
				</div>
			</div>
		</div>
	</div>
	<!-- //video and events -->
<!--footer-->
	<footer>
		<div class="container py-md-4 mt-md-3">
			<div class="row footer-top-w3layouts-agile py-5">
				<div class="col-md-4 footer-grid">
					<div class="footer-title">
						<h3>About Us</h3>
					</div>
					<div class="footer-text">
						<p>Balai Latihan Kerja atau sering disebut dengan singkatan BLK adalah prasarana dan sarana tempat pelatihan untuk mendapatkan keterampilan atau yang ingin mendalami keahlian dibidangnya masing-masing.</p>
					</div>
				</div>
				<div class="col-md-4 footer-grid">
					<div class="footer-title">
						<h3>Contact Us</h3>
					</div>
					<div class="contact-info">
					<h4>Location :</h4>
					<p>Dinas Tenaga Kerja dan Transmigrasi Provinsi Jawa Timur</p>
					<div class="phone">
						<h4>Phone :</h4>
						<p>Phone : (031) 8280254</p>
						<p>Email : <a href="mailto:info@example.com">info@example.com</a></p>
					</div>
				</div>
				</div>
				<div class="col-md-4 footer-grid">
					<div class="footer-title">
						<h3>Recent Posts</h3>
					</div>
					<div class="footer-list">
						<div class="flickr-grid">
							<a href="services.html">
								<img src="images/g1.jpg" class="img-fluid" alt=" ">
							</a>
						</div>
						<div class="flickr-grid">
							<a href="services.html">
								<img src="images/g2.jpg" class="img-fluid" alt=" ">
							</a>
						</div>
						<div class="flickr-grid">
							<a href="services.html">
								<img src="images/g3.jpg" class="img-fluid" alt=" ">
							</a>
						</div>
						<div class="flickr-grid">
							<a href="services.html">
								<img src="images/g4.jpg" class="img-fluid" alt=" ">
							</a>
						</div>
						<div class="flickr-grid">
							<a href="services.html">
								<img src="images/g5.jpg" class="img-fluid" alt=" ">
							</a>
						</div>
						<div class="flickr-grid">
							<a href="services.html">
								<img src="images/g6.jpg" class="img-fluid" alt=" ">
							</a>
						</div>
						<div class="flickr-grid">
							<a href="services.html">
								<img src="images/g7.jpg" class="img-fluid" alt=" ">
							</a>
						</div>
						<div class="flickr-grid">
							<a href="services.html">
								<img src="images/g9.jpg" class="img-fluid" alt=" ">
							</a>
						</div>
						<div class="flickr-grid">
							<a href="services.html">
								<img src="images/g8.jpg" class="img-fluid" alt=" ">
							</a>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				
			</div>
		</div>
	</footer>
	<!---->
	<div class="copyright py-3">
		<div class="container">
			<div class="copyrighttop">
				<ul>
					<li>
						<h4>Follow us on:</h4>
					</li>
					<li>
						<a href="#">
							<span class="fa fa-facebook"></span>
						</a>
					</li>
					<li>
						<a href="#">
							<span class="fa fa-twitter"></span>
						</a>
					</li>
					<li>
						<a href="#">
							<span class="fa fa-google-plus"></span>
						</a>
					</li>
					<li>
						<a href="#">
							<span class="fa fa-pinterest"></span>
						</a>
					</li>
				</ul>
			</div>
			<div class="copyrightbottom">
				<p>© 2020 Balai Latihan Kerja  | Design by
					<a href="https://inzaghigigantara.net/">Inzaghi Gigantara Solusindo</a>
				</p>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
<!-- //footer -->

<script type="text/javascript">
            $(document).ready(function () {
                $('.fancybox').fancybox({
                    'width': '80%',
                    'height': '500',
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                    'type': 'iframe'
                });

            });
        </script>

</body>
</html>