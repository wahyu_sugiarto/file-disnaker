<?php
$kota = $_GET['name'];
$tahun = $_GET['tahun'];
$tahun_echo = $_GET['tahun'];

$url = "http://localhost/Api_Disnaker/WebService/getDataPerTahunBlkCity/" . $kota . "/" . $tahun;
$client = curl_init($url);
curl_setopt($client, CURLOPT_RETURNTRANSFER, 1);
$response = curl_exec($client);
$result = json_decode($response, true);

$namaProgram = array();
$totalPeserta = array();
foreach ($result['data_pelatihan'] as $key => $value) {
    $namaProgram[] = $value['nama_program'];
    $totalPeserta[] = (float) $value['jml_peserta'];
}

$dataSeriesPeserta[] = array(
    'name' => 'Data Pelatihan',
    'shadow' => true,
    'data' => $totalPeserta,
);

$tahun = array();
$jmlPeserta = array();
$jmlLulusan = array();
foreach ($result['data_pelatihan_lulus'] as $key => $value) {
    $tahun[] = $value['tahun'];
    $jmlPeserta[] = (float) $value['jml_peserta'];
    $jmlLulusan[] = (float) $value['jml_lulusan'];
}

$dataSeriesLulus[] = array(
    'name' => 'Jumlah Peserta',
    'shadow' => true,
    'data' => $jmlPeserta,
);
$dataSeriesLulus[] = array(
    'name' => 'Jumlah Lulusan',
    'shadow' => true,
    'data' => $jmlLulusan,
);

$tahunSertifikasi = array();
$jmlPesertaSertifikasi = array();
$jmlLulusanSertifikasi = array();
foreach ($result['data_pelatihan_sertifikasi_lulus'] as $key => $value) {
    $tahunSertifikasi[] = $value['tahun'];
    $jmlPesertaSertifikasi[] = (float) $value['jml_peserta_sertifikasi'];
    $jmlLulusanSertifikasi[] = (float) $value['jml_lulusan_sertifikasi'];
}

$dataSeriesLulusSertifikasi[] = array(
    'name' => 'Jumlah Peserta Sertifikasi',
    'shadow' => true,
    'data' => $jmlPesertaSertifikasi,
);
$dataSeriesLulusSertifikasi[] = array(
    'name' => 'Jumlah Lulusan Sertifikasi',
    'shadow' => true,
    'data' => $jmlLulusanSertifikasi,
);

?>
<html>
    <head>
        <link rel="stylesheet" href="assets/gaya.css" />
        <link rel='stylesheet' href='assets/css/font-awesome.css'>

        <script src="assets/jquery-3.3.1.js"></script>
        <link rel="stylesheet" href="assets/popup/jquery.fancybox.css" />
        <link rel="stylesheet" href="assets/gaya.css" />
        <script src="assets/popup/jquery.fancybox.js"></script>
        <script src = "assets/highchart/highcharts.js"></script>
        <script src="assets/highchart/modules/exporting.js"></script>
        <script src="assets/highchart/modules/offline-exporting.js"></script>
        <script>
            function goBack() {
                window.history.back();
            }
        </script>

        <style type="text/css">
            .bagan{
                width: 100%; 
            }
            h3{
                margin: 0px !important;
                font-family: 'Poppins', sans-serif;
                color: #000;
            }

            .gap {margin-top:20px;}

            h3 i{
                font-size: 22.5px;
                margin-right: 3px;
                color: #d32f2f;
            }

            h3 span{
                color: #d32f2f;
                font-weight: bold;
            }

            p{
                margin: 4px 0px 4px 0px !important;
                font-family: 'Raleway', sans-serif;
                font-size: 13.5px !important;
                color: #212121;
            }

            table > tbody > tr > td{
                padding: 4px 15px 4px 0px !important;
                border-top: transparent;
                font-family: 'Raleway', sans-serif;
                vertical-align: top;
                font-size: 13.5px !important;
                color: #212121;
            }

            .donlod{color: #388E3C !important;}

            .donlod:hover, .donlod:focus, .donlod:active{color: #2E7D32 !important;}

            .popupBody{margin: 10px !important;}

            .title{
                color: #6D4C41;
                font-weight: bold;
            }

            .kembali{
                border-radius: 2px !important;
                padding: 10px 16.5px !important;
                font-family: 'Raleway', sans-serif !important;
                font-size: 13px;
                margin: 15px 0px 12px 0px;
                background-color: #424242 !important;
                color: #fff !important;
                border:transparent;
                position: absolute;
            }

            .kembali:hover,
            .kembali:focus,
            .kembali:active{background-color: #212121 !important;}

            .kembali i{
                margin-right: 10px;
                font-size: 11.5px;
            }

            .notice{
                font-weight: bold;
                margin-top: 15px;
                font-family: 'Raleway', sans-serif;
                font-size: 13.5px !important;
                color: #d32f2f;
            }
        </style>
    </head>
    <body>
        <div class="bagan">
            <header class="popupHeader">
                <h3><i class="fa fa-copy"></i>  Data <span>UPT BLK <?php echo ucfirst($kota); ?> Tahun <?php echo $tahun_echo; ?> </span></h3>
            </header>
            <section class="popupBody">
                <div class="row">
                    <div class="gap"></div>
                    <div class="col-lg-12">
                        <div id="report_pelatihan"></div>
                    </div>
                    <div class="gap"></div>
                    <div class="col-lg-12">
                        <div id="report_pelatihan_lulus"></div>
                    </div>
                    <div class="gap"></div>
                    <div class="col-lg-12">
                        <div id="report_pelatihan_lulus_sertifikasi"></div>
                    </div>
                </div>
            </section>
            <div class="notice">
                Tolong teliti dan dibaca baik - baik akan informasi data diatas, agar tidak kesalahan informasi Terimakasih !
            </div>
            <button class="btn kembali" onclick="goBack()">
                <i class="fa fa-chevron-left"></i>Kembali
            </button>
        </div>
        <script language = "JavaScript">
            $(document).ready(function () {
                // Start Pelatihan
                var title_pelatihan = 'Data Grafik Pelatihan';
                var y_title_pelatihan = 'Total Peserta';
                var categories_program = <?php echo json_encode($namaProgram) ?>;
                var data_series_peserta = <?php echo json_encode($dataSeriesPeserta) ?>;

                setChart('report_pelatihan', title_pelatihan, y_title_pelatihan, categories_program, data_series_peserta);
                // End of Pelatihan

                // Start Diterima
                var title_pelatihan_lulus = 'Data Grafik Jumlah Peserta dan Lulusan';
                var y_title_peserta = 'Total Peserta dan Lulusan';
                var categories_tahun = <?php echo json_encode($tahun) ?>;
                var data_series_lulusan = <?php echo json_encode($dataSeriesLulus) ?>;

                setChart('report_pelatihan_lulus', title_pelatihan_lulus, y_title_peserta, categories_tahun, data_series_lulusan);

                // Start Sertifikasi
                var title_pelatihan_lulus_sertifikasi = 'Data Grafik Jumlah Peserta Sertifikasi dan Lulusan Sertifikasi';
                var y_title_peserta_sertifikasi = 'Total Peserta Sertifikasi dan Lulusan Sertifikasi';
                var categories_tahun_sertifikasi = <?php echo json_encode($tahunSertifikasi) ?>;
                var data_series_lulusan_sertifikasi = <?php echo json_encode($dataSeriesLulusSertifikasi) ?>;

                setChart('report_pelatihan_lulus_sertifikasi', title_pelatihan_lulus_sertifikasi, y_title_peserta_sertifikasi, categories_tahun_sertifikasi, data_series_lulusan_sertifikasi);
            });

            function setChart(classaName, mainTitle, yTitle, categories, dataSeries) {
//                console.log(dataSeries);
                var chart = {
                    type: 'column'
                };
                var title = {
                    text: mainTitle
                };
                var xAxis = {
                    categories: categories,
                    crosshair: true
                };
                var yAxis = {
                    min: 0,
                    title: {
                        text: yTitle
                    }
                };
                var tooltip = {
                    shared: true,
                    useHTML: true
                };
                var plotOptions = {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                };
                var credits = {
                    enabled: false
                };
                var series = dataSeries;

                var json = {};
                json.chart = chart;
                json.title = title;
                json.tooltip = tooltip;
                json.xAxis = xAxis;
                json.yAxis = yAxis;
                json.series = series;
                json.plotOptions = plotOptions;
                json.credits = credits;
                $('#' + classaName).highcharts(json);
            }
        </script>
    </body>
</html>