<?php
$kota = $_GET['name'];

$url = "http://localhost/Api_Disnaker/WebService/getDataBlkCity/" . $kota;
$client = curl_init($url);
curl_setopt($client, CURLOPT_RETURNTRANSFER, 1);
$response = curl_exec($client);
$result = json_decode($response, true);

?>

<html>
    <head>
        <link rel="stylesheet" href="assets/dataTable/css/datatable.css" />
        <link rel="stylesheet" href="assets/dataTable/css/datatable2.min.css" />
        <link rel="stylesheet" href="assets/gaya.css" />
        <link rel='stylesheet' href='assets/css/font-awesome.css'>
        <link rel="stylesheet" href="assets/popup/jquery.fancybox.css" />
        <link rel="stylesheet" href="assets/gaya.css" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

        <script src="assets/jquery-3.3.1.js"></script>
        <script src="assets/popup/jquery.fancybox.js"></script>
        <script src = "assets/highchart/highcharts.js"></script>
        <script src=" assets/highchart/modules/exporting.js"></script>
        <script src="assets/highchart/modules/offline-exporting.js"></script>
        <script src="assets/dataTable/js/dataTables.min.js"></script>
        <script src="assets/dataTable/js/dataTables2.min.js"></script>
        <script src="assets/dataTable/js/dataTables.buttons.min.js"></script>
        <script src="assets/dataTable/js/pdfmake.min.js"></script>
        <script src="assets/dataTable/js/vfs_fonts.js"></script>
        <script src="assets/dataTable/js/buttons.html5.min.js"></script>
        <script src="assets/dataTable/js/buttons.flash.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <script>
            function goBack() {
                window.history.back();
            }
        </script>
        <style type="text/css">
            .bagan{width: 100%; }
            h3{
                margin: 0px !important;
                font-family: 'Poppins', sans-serif;
                color: #000;
            }
            h3 i{
                font-size: 22.5px;
                margin-right: 3px;
                color: #d32f2f;
            }
            h3 span{
                color: #d32f2f;
                font-weight: bold;
            }
            p{
                margin: 4px 0px 4px 0px !important;
                font-family: 'Raleway', sans-serif;
                font-size: 13.5px !important;
                color: #212121;
            }
            .gap {margin-top:20px;}
            table > tbody > tr > td{
                padding: 4px 15px 4px 0px !important;
                border-top: transparent;
                font-family: 'Raleway', sans-serif;
                vertical-align: top;
                font-size: 12px !important;
                color: #212121;
            }
            .donlod{color: #388E3C !important;}
            .donlod:hover, .donlod:focus, .donlod:active{color: #2E7D32 !important;}
            .popupBody{margin: 10px !important;}
            .title{
                color: #6D4C41;
                font-weight: bold;
            }
            .kembali{
                border-radius: 2px !important;
                padding: 10px 16.5px !important;
                font-family: 'Raleway', sans-serif !important;
                font-size: 13px;
                margin: 15px 0px 12px 0px;
                background-color: #424242 !important;
                color: #fff !important;
                border:transparent;
                position: absolute;
            }
            .kembali:hover,
            .kembali:focus,
            .kembali:active{background-color: #212121 !important;}
            .kembali i{
                margin-right: 10px;
                font-size: 11.5px;
            }
            .notice{
                font-weight: bold;
                margin-top: 15px;
                font-family: 'Raleway', sans-serif;
                font-size: 13.5px !important;
                color: #d32f2f;
            }
        </style>
    </head>
    <body>
        <div class="bagan">
            <header class="popupHeader">
                <h3><i class="fa fa-copy"></i>  Data <span>UPT BLK <?php echo ucfirst($kota); ?></span></h3>
                <p>Berikut ini adalah data dari salah satu list data yang anda pilih.</p>
            </header>
            <section class="popupBody">
                <div class="row">
                    <div class="gap"></div>
                    <div class="col-lg-12">
                        <h4 style="text-align: center;">Data Instruktur</h4>
                        Jumlah Instruktur Pria : <?php
                        if (!empty($result['data_instructure']['pria'])) {
                            echo count($result['data_instructure']['pria']);
                        } else {
                            echo "0";
                        }

                        ?> orang.
                        <div class="gap"></div>
                        <table id="tabelku" class="table table-striped table-bordered tabelku" style="width:100%">
                            <thead>
                                <tr style="text-align: center;">
                                    <td>Nama</td>
                                    <td>Tanggal Lahir</td>
                                    <td>Alamat Rumah</td>
                                    <!-- <td>telepon</td> -->
                                    <td>Kejuruan</td>
                                    <td>Sub Kejuruan</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (!empty($result['data_instructure']['pria'])) { ?>
                                    <?php foreach ($result['data_instructure']['pria'] as $key => $value) { ?>
                                        <tr>
                                            <td><?php echo $value['name']; ?></td>
                                            <td style="text-align: center;"><?php echo date('d-m-Y', strtotime($value['tanggal_lahir'])); ?></td>
                                            <td><?php echo $value['alamat_rumah']; ?></td>
                                            <!-- <td><?php echo $value['telepon']; ?></td> -->
                                            <td><?php echo $value['kejuruan']; ?></td>
                                            <td><?php echo $value['kejuruan_line']; ?></td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="gap"></div>
                    <div class="col-lg-12">
                        <h4 style="text-align: center;">&nbsp;</h4>
                        Jumlah Instruktur Wanita : <?php
                        if (!empty($result['data_instructure']['wanita'])) {
                            echo count($result['data_instructure']['wanita']);
                        } else {
                            echo "0";
                        }

                        ?> orang.
                        <div class="gap"></div>
                        <table id="tabelku" class="table table-striped table-bordered tabelku" style="width:100%">
                            <thead>
                                <tr style="text-align: center;">
                                    <td>Nama</td>
                                    <td>Tanggal Lahir</td>
                                    <td>Alamat Rumah</td>
                                   <!--  <td>telepon</td> -->
                                    <td>Kejuruan</td>
                                    <td>Sub Kejuruan</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (!empty($result['data_instructure']['wanita'])) { ?>
                                    <?php foreach ($result['data_instructure']['wanita'] as $key => $value) { ?>
                                        <tr>
                                            <td><?php echo $value['name']; ?></td>
                                            <td style="text-align: center;"><?php echo date('d-m-Y', strtotime($value['tanggal_lahir'])); ?></td>
                                            <td><?php echo $value['alamat_rumah']; ?></td>
                                            <!-- <td><?php echo $value['telepon']; ?></td> -->
                                            <td><?php echo $value['kejuruan']; ?></td>
                                            <td><?php echo $value['kejuruan_line']; ?></td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="gap"></div>
                    <div class="col-lg-12">
                        <h4 style="text-align: center;">Data Peserta</h4>
                        Jumlah Peserta Pria : <?php
                        if (!empty($result['data_peserta']['pria'])) {
                            echo count($result['data_peserta']['pria']);
                        } else {
                            echo "0";
                        }

                        ?> orang.
                        <div class="gap"></div>
                        <table id="tabelku2" class="table table-striped table-bordered tabelku" style="width:100%">
                            <thead>
                                <tr style="text-align: center;">
                                    
                                    <td>Nama</td>
                                    <td>Tanggal Lahir</td>
                                    <td>Tempat Lahir</td>
                                    <td>Alamat</td>
                                    <td>Nomor Pendaftaran</td>
                                    <td>Tanggal Pendaftaran</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (!empty($result['data_peserta']['pria'])) { ?>
                                    <?php foreach ($result['data_peserta']['pria'] as $key => $value) { ?>
                                        <tr>
                                            <td><?php echo $value['name']; ?></td>
                                            <td style="text-align: center;"><?php echo (strlen($value['tgl_lahir']) > 0) ? date('d-m-Y', strtotime($value['tgl_lahir'])) : ""; ?></td>
                                            <td><?php echo $value['tempat_lahir']; ?></td>
                                            <td><?php echo $value['alamat']; ?></td>
                                            <td><?php echo $value['nomor_pendaftaran']; ?></td>
                                            <td style="text-align: center;"><?php echo date('d-m-Y', strtotime($value['tgl_pendaftaran'])); ?></td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="gap"></div>
                    <div class="col-lg-12">
                        <h4 style="text-align: center;">&nbsp;</h4>
                        Jumlah Peserta Wanita : <?php
                        if (!empty($result['data_peserta']['wanita'])) {
                            echo count($result['data_peserta']['wanita']);
                        } else {
                            echo "0";
                        }

                        ?> orang.
                        <div class="gap"></div>
                        <table id="tabelku2" class="table table-striped table-bordered tabelku" style="width:100%">
                            <thead>
                                <tr style="text-align: center;">
                                    <td>Nama</td>
                                    <td>Tanggal Lahir</td>
                                    <td>Tempat Lahir</td>
                                    <td>Alamat</td>
                                    <td>Nomor Pendaftaran</td>
                                    <td>Tanggal Pendaftaran</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (!empty($result['data_peserta']['wanita'])) { ?>
                                    <?php foreach ($result['data_peserta']['wanita'] as $key => $value) { ?>
                                        <tr>
                                            <td><?php echo $value['name']; ?></td>
                                            <td style="text-align: center;"><?php echo (strlen($value['tgl_lahir']) > 0) ? date('d-m-Y', strtotime($value['tgl_lahir'])) : ""; ?></td>
                                            <td><?php echo $value['tempat_lahir']; ?></td>
                                            <td><?php echo $value['alamat']; ?></td>
                                            <td><?php echo $value['nomor_pendaftaran']; ?></td>
                                            <td style="text-align: center;"><?php echo date('d-m-Y', strtotime($value['tgl_pendaftaran'])); ?></td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="gap"></div>
                    <div class="col-lg-12">
                        <h4 style="text-align: center;">Data Kejuruan</h4>
                        Jumlah Kejuruan : <?php
                        if (!empty($result['data_kejuruan'])) {
                            echo count($result['data_kejuruan']);
                        } else {
                            echo "0";
                        }

                        ?>.
                        <div class="gap"></div>
                        <table id="tabelku3" class="table table-striped table-bordered tabelku" style="width:100%">
                            <thead>
                                <tr style="text-align: center;">
                                    <td>Kejuruan</td>
                                    <td>Sub Kejuruan</td>
                                    <td>Standar yang Digunakan</td>
                                    <td>Kapasitas Melatih</td>
                                    <td>Jml Instruktur yg Dibutuhkan</td>
                                    <td>Tempat Uji Kompetensi</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (!empty($result['data_kejuruan'])) { ?>
                                    <?php foreach ($result['data_kejuruan'] as $key => $value) { ?>
                                        <tr>
                                            <td><?php echo $value['kejuruan']; ?></td>
                                            <td><?php echo $value['kejuruan_line']; ?></td>
                                            <td><?php echo $value['standar']; ?></td>
                                            <td style="text-align: right;"><?php echo $value['kapasitas_melatih']; ?></td>
                                            <td style="text-align: right;">
                                                <?php
                                                if ($value['instruktur_dibutuhkan'] != null): echo $value['instruktur_dibutuhkan'];
                                                else: echo '0';
                                                endif;

                                                ?>
                                            </td>
                                            <td style="text-align: center;">
                                                <?php
                                                if ($value['tuk'] == 't'): echo '<span class="glyphicon glyphicon-check">';
                                                elseif ($value['tuk'] == 'f') : echo '<span class="glyphicon glyphicon-unchecked">';
                                                else: echo '';
                                                endif;

                                                ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="gap"></div>
                    <div class="col-lg-12">
                        <h4 style="text-align: center;">Data Sarana dan Prasarana</h4>
                        Jumlah Sarana dan Prasarana : <?php
                        if (!empty($result['data_sarana_prasarana'])) {
                            echo count($result['data_sarana_prasarana']);
                        } else {
                            echo "0";
                        }

                        ?>.
                        <div class="gap"></div>
                        <table id="tabelku3" class="table table-striped table-bordered tabelku" style="width:100%">
                            <thead>
                                <tr style="text-align: center;">
                                    <td>Nama</td>
                                    <td>Jenis</td>
                                    <td>Kondisi</td>
                                    <td>Jumlah Sekarang</td>
                                    <td>Jumlah yang Dibutuhkan</td>
                                    <td>Kejuruan</td>
                                    <td>Sub Kejuruan</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (!empty($result['data_instructure'])) { ?>
                                    <?php foreach ($result['data_sarana_prasarana'] as $key => $value) { ?>
                                        <tr>
                                            <td><?php echo $value['name']; ?></td>
                                            <td><?php echo $value['jenis']; ?></td>
                                            <td><?php echo $value['kondisi']; ?></td>
                                            <td style="text-align: right;"><?php echo $value['jml_sekarang']; ?></td>
                                            <td style="text-align: right;"><?php echo $value['jml_yg_dibutuhkan']; ?></td>
                                            <td><?php echo $value['kejuruan']; ?></td>
                                            <td><?php echo $value['kejuruan_line']; ?></td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
            <div class="gap"></div>
            <div class="notice">
                Tolong teliti dan dibaca baik - baik akan informasi data diatas, agar tidak kesalahan informasi Terimakasih !
            </div>
            <button class="btn kembali" onclick="goBack()">
                <i class="fa fa-chevron-left"></i>Kembali
            </button>
        </div>
    </body>
</html> 

<script type="text/javascript">
    $(document).ready(function () {
        $('.tabelku').DataTable({
            oLanguage: {
                "sLengthMenu": "_MENU_ &nbsp;&nbsp;Data Per Halaman",
                "sInfo": "Menampilkan _START_ s/d _END_ dari <b>_TOTAL_ data</b>",
                "sInfoFiltered": "(difilter dari _MAX_ total data)",
                "sEmptyTable": "Data tidak ada di server",
                "sInfoPostFix": "",
                "sSearch": "<i class='fa fa-search fa-fw'></i> Pencarian : ",
                "sPaginationType": "simple_numbers",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext": "Selanjutnya",
                    "sLast": "Terakhir"
                }
            }
        });
//        $('.tabelku').DataTable( {
//            dom: 'Bfrtip',
//            buttons: [
//                 {
//                    extend: 'pdf',
//                    title: 'Export PDF',
//                },
//            ]
//        });
    });
</script>