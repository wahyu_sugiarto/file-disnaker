<?php
$kota = $_GET['name'];
$sumberDana = $_GET['sumber_dana'];
$tahun = $_GET['tahun'];

$url = "http://localhost/Api_Disnaker/WebService/detailApbnApbdSwadana/" . $kota . "/" . $sumberDana . "/" . $tahun;
$client = curl_init($url);
curl_setopt($client, CURLOPT_RETURNTRANSFER, 1);
$response = curl_exec($client);
$result = json_decode($response, true);

$arrResultInstitusional = array();
$arrResultNonInstitusional = array();
foreach ($result['data_apbn_apbd_swadana'] as $key => $value) {
    if ($value['metode_pelatihan'] == 'INSTITUSIONAL') {
        $arrResultInstitusional[] = $value;
    } else if ($value['metode_pelatihan'] == 'MTU / NON INSTITUSIONAL') {
        $arrResultNonInstitusional[] = $value;
    }
}

?>

<html>
    <head>
        <link rel="stylesheet" href="assets/dataTable/css/datatable.css" />
        <link rel="stylesheet" href="assets/dataTable/css/datatable2.min.css" />
        <link rel="stylesheet" href="assets/gaya.css" />
        <link rel='stylesheet' href='assets/css/font-awesome.css'>
        <link rel="stylesheet" href="assets/popup/jquery.fancybox.css" />
        <link rel="stylesheet" href="assets/gaya.css" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

        <script src="assets/jquery-3.3.1.js"></script>
        <script src="assets/popup/jquery.fancybox.js"></script>
        <script src = "assets/highchart/highcharts.js"></script>
        <script src=" assets/highchart/modules/exporting.js"></script>
        <script src="assets/highchart/modules/offline-exporting.js"></script>
        <script src="assets/dataTable/js/dataTables.min.js"></script>
        <script src="assets/dataTable/js/dataTables2.min.js"></script>
        <script src="assets/dataTable/js/dataTables.buttons.min.js"></script>
        <script src="assets/dataTable/js/pdfmake.min.js"></script>
        <script src="assets/dataTable/js/vfs_fonts.js"></script>
        <script src="assets/dataTable/js/buttons.html5.min.js"></script>
        <script src="assets/dataTable/js/buttons.flash.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <script>
            function goBack() {
                window.history.back();
            }
        </script>
        <style type="text/css">
            .bagan{width: 100%; }
            h3{
                margin: 0px !important;
                font-family: 'Poppins', sans-serif;
                color: #000;
            }
            h3 i{
                font-size: 22.5px;
                margin-right: 3px;
                color: #d32f2f;
            }
            h3 span{
                color: #d32f2f;
                font-weight: bold;
            }
            p{
                margin: 4px 0px 4px 0px !important;
                font-family: 'Raleway', sans-serif;
                font-size: 13.5px !important;
                color: #212121;
            }
            .gap {margin-top:20px;}
            table > tbody > tr > td{
                padding: 4px 15px 4px 0px !important;
                border-top: transparent;
                font-family: 'Raleway', sans-serif;
                vertical-align: top;
                font-size: 12px !important;
                color: #212121;
            }
            .donlod{color: #388E3C !important;}
            .donlod:hover, .donlod:focus, .donlod:active{color: #2E7D32 !important;}
            .popupBody{margin: 10px !important;}
            .title{
                color: #6D4C41;
                font-weight: bold;
            }
            .kembali{
                border-radius: 2px !important;
                padding: 10px 16.5px !important;
                font-family: 'Raleway', sans-serif !important;
                font-size: 13px;
                margin: 15px 0px 12px 0px;
                background-color: #424242 !important;
                color: #fff !important;
                border:transparent;
                position: absolute;
            }
            .kembali:hover,
            .kembali:focus,
            .kembali:active{background-color: #212121 !important;}
            .kembali i{
                margin-right: 10px;
                font-size: 11.5px;
            }
            .notice{
                font-weight: bold;
                margin-top: 15px;
                font-family: 'Raleway', sans-serif;
                font-size: 13.5px !important;
                color: #d32f2f;
            }
        </style>
    </head>
    <body>
        <div class="bagan">
            <header class="popupHeader">
                <h3><i class="fa fa-copy"></i>  Data <span>UPT BLK <?php echo ucfirst($kota); ?></span></h3>
                <p>Berikut ini adalah data dari salah satu list data yang anda pilih.</p>
            </header>
            <section class="popupBody">
                <div class="row">
                    <div class="gap"></div>
                    <div class="col-lg-12">
                        <h4 style="text-align: center;">Data Pelatihan <?php echo strtoupper($sumberDana); ?> Institusional</h4>
                         <!-- Jumlah Institusional : --> <?php
                        // if (!empty($arrResultInstitusional)) {
                        //     echo count($arrResultInstitusional);
                        // } else {
                        //     echo "0";
                        // }

                        ?><!-- . -->
                        <div class="gap"></div>
                        <table id="tabelku" class="table table-striped table-bordered tabelku" style="width:100%">
                            <thead>
                                <tr style="text-align: center;">
                                    <td>Kejuruan</td>
                                    <td>Jumlah Pelatihan</td>
                                    <td>Jumlah Peserta</td>
                                    <td>Jumlah Lulusan</td>
                                    <td>Jumlah Penempatan</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (!empty($arrResultInstitusional)) { ?>
                                    <?php foreach ($arrResultInstitusional as $key => $value) { ?>
                                        <tr>
                                            <td><?php echo $value['kejuruan']; ?></td>
                                            <td><?php echo $value['jml_pelatihan']; ?></td>
                                            <td><?php echo $value['jml_peserta']; ?></td>
                                            <td><?php echo $value['jml_lulusan']; ?></td>
                                            <td><?php echo $value['jml_penempatan']; ?></td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="gap"></div>
                    <div class="col-lg-12">
                        <h4 style="text-align: center;">Data Pelatihan <?php echo strtoupper($sumberDana); ?> MTU / Non Institusional</h4>
                        <!-- Jumlah MTU / NO Institusional : --> <?php
                        // if (!empty($arrResultNonInstitusional)) {
                        //     echo count($arrResultNonInstitusional);
                        // } else {
                        //     echo "0";
                        // }

                        ?><!-- . -->
                        <div class="gap"></div>
                        <table id="tabelku" class="table table-striped table-bordered tabelku" style="width:100%">
                            <thead>
                                <tr style="text-align: center;">
                                    <td>Kejuruan</td>
                                    <td>Jumlah Pelatihan</td>
                                    <td>Jumlah Peserta</td>
                                    <td>Jumlah Lulusan</td>
                                    <td>Jumlah Penempatan</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (!empty($arrResultNonInstitusional)) { ?>
                                    <?php foreach ($arrResultNonInstitusional as $key => $value) { ?>
                                        <tr>
                                            <td><?php echo $value['kejuruan']; ?></td>
                                            <td><?php echo $value['jml_pelatihan']; ?></td>
                                            <td><?php echo $value['jml_peserta']; ?></td>
                                            <td><?php echo $value['jml_lulusan']; ?></td>
                                            <td><?php echo $value['jml_penempatan']; ?></td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
            <div class="notice">
                Tolong teliti dan dibaca baik - baik akan informasi data diatas, agar tidak kesalahan informasi Terimakasih !
            </div>
            <button class="btn kembali" onclick="goBack()">
                <i class="fa fa-chevron-left"></i>Kembali
            </button>
        </div>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.tabelku').DataTable({
                    oLanguage: {
                        "sLengthMenu": "_MENU_ &nbsp;&nbsp;Data Per Halaman",
                        "sInfo": "Menampilkan _START_ s/d _END_ dari <b>_TOTAL_ data</b>",
                        "sInfoFiltered": "(difilter dari _MAX_ total data)",
                        "sEmptyTable": "Data tidak ada di server",
                        "sInfoPostFix": "",
                        "sSearch": "<i class='fa fa-search fa-fw'></i> Pencarian : ",
                        "sPaginationType": "simple_numbers",
                        "sUrl": "",
                        "oPaginate": {
                            "sFirst": "Pertama",
                            "sPrevious": "Sebelumnya",
                            "sNext": "Selanjutnya",
                            "sLast": "Terakhir"
                        }
                    }
                });
            });
        </script>
    </body>
</html>	