<?php
$kota = $_GET['name'];

$url = "http://localhost/Api_Disnaker/WebService/getDataBlkCity/" . $kota;
$client = curl_init($url);
curl_setopt($client, CURLOPT_RETURNTRANSFER, 1);
$response = curl_exec($client);
$result = json_decode($response, true);

$tahunDiterima = array();
$totalDiterima = array();

if (!empty($result['total_diterima'])) {

    foreach ($result['total_diterima'] as $key => $value) {
        $tahunDiterima[] = $value['tahun'];
        $totalDiterima[] = (float) $value['total_diterima'];
    }
} else {
    echo "Data Kosong";
}


$dataDiterima[] = array(
    'name' => 'Data',
    'shadow' => true,
    'data' => $totalDiterima,
);

$tahunPeserta = array();
$totalPeserta = array();

if (!empty($result['total_peserta'])) {
    foreach ($result['total_peserta'] as $key => $value) {
        $tahunPeserta[] = $value['tahun'];
        $totalPeserta[] = (float) $value['jml_peserta'];
    }
} else {
    echo "Data Kosong";
}
$dataPeserta[] = array(
    'name' => 'Data',
    'shadow' => true,
    'data' => $totalPeserta,
);

$tahunPelatihan = array();
$totalPelatihan = array();


if (!empty($result['total_peserta'])) {
    foreach ($result['total_pelatihan'] as $key => $value) {
        $tahunPelatihan[] = $value['tahun'];
        $totalPelatihan[] = (float) $value['jml_pelatihan'];
    }
} else {
    echo "Data Kosong";
}

$dataPelatihan[] = array(
    'name' => 'Data',
    'shadow' => true,
    'data' => $totalPelatihan,
);

$jenis = array('Sarana', 'Prasarana');
$jmlSkrg = array();
$jmlDibutuhkan = array();

if (!empty($result['total_sarana_prasarana'])) {

    foreach ($result['total_sarana_prasarana'] as $key => $value) {
        $jmlSkrg[] = (float) $value['jml_sekarang'];
        $jmlDibutuhkan[] = (float) $value['jml_yg_dibutuhkan'];
    }
} else {
    echo "Data Kosong";
}

$dataSaranaPrasarana[] = array(
    'name' => 'Jumlah Sekarang',
    'shadow' => true,
    'data' => $jmlSkrg,
);
$dataSaranaPrasarana[] = array(
    'name' => 'Jumlah yang Dibutuhkan',
    'shadow' => true,
    'data' => $jmlDibutuhkan,
);

?>

<html>
    <head>
        <link rel="stylesheet" href="assets/gaya.css" />
        <link rel='stylesheet' href='assets/css/font-awesome.css'>

        <script src="assets/jquery-3.3.1.js"></script>
        <link rel="stylesheet" href="assets/popup/jquery.fancybox.css" />
        <link rel="stylesheet" href="assets/gaya.css" />
        <script src="assets/popup/jquery.fancybox.js"></script>

        <script src = "assets/highchart/highcharts.js"></script>
        <script src=" assets/highchart/modules/exporting.js"></script>
        <script src="assets/highchart/modules/offline-exporting.js"></script>

        <script>
            function goBack() {
                window.history.back();
            }
        </script>

        <style type="text/css">
            .bagan{
                width: 10o%; 
            }
            h3{
                margin: 0px !important;
                font-family: 'Poppins', sans-serif;
                color: #000;
            }
            h3 i{
                font-size: 22.5px;
                margin-right: 3px;
                color: #d32f2f;
            }
            h3 span{
                color: #d32f2f;
                font-weight: bold;
            }
            p{
                margin: 4px 0px 4px 0px !important;
                font-family: 'Raleway', sans-serif;
                font-size: 13.5px !important;
                color: #212121;
            }
            .gap {margin-top:20px;}
            table > tbody > tr > td{
                padding: 4px 15px 4px 0px !important;
                border-top: transparent;
                font-family: 'Raleway', sans-serif;
                vertical-align: top;
                font-size: 13.5px !important;
                color: #212121;
            }
            .donlod{color: #388E3C !important;}
            .donlod:hover, .donlod:focus, .donlod:active{color: #2E7D32 !important;}
            .popupBody{margin: 10px !important;}
            .title{
                color: #6D4C41;
                font-weight: bold;
            }
            .kembali{
                border-radius: 2px !important;
                padding: 10px 16.5px !important;
                font-family: 'Raleway', sans-serif !important;
                font-size: 13px;
                margin: 15px 0px 12px 0px;
                background-color: #424242 !important;
                color: #fff !important;
                border:transparent;
                position: absolute;
            }
            .kembali:hover,
            .kembali:focus,
            .kembali:active{background-color: #212121 !important;}
            .kembali i{
                margin-right: 10px;
                font-size: 11.5px;
            }
            .notice{
                font-weight: bold;
                margin-top: 15px;
                font-family: 'Raleway', sans-serif;
                font-size: 13.5px !important;
                color: #d32f2f;
            }
        </style>
    </head>
    <body>
        <div class="bagan">
            <header class="popupHeader">
                <h3><i class="fa fa-copy"></i>  Data <span>UPT BLK <?php echo ucfirst($kota); ?></span></h3>
                <p>Berikut ini adalah data dari salah satu list data yang anda pilih.</p>
            </header>
            <section class="popupBody">
                <?php
                if (!empty($result['tahun'])) {
                    foreach ($result['tahun'] as $data) {
                        echo "<a href='detail_kabupaten.php?name=" . $kota . "&tahun=" . $data['tahun'] . "'><button class='w3-btn w3-red'><i class='fa fa-info-circle'></i> " . $data['tahun'] . "</button></a>&nbsp ";
                    }
                } else {
                    echo "Data Kosong";
                }

                ?>
                <div class="row">
                    <div class="gap"></div>
                    <div class="col-lg-12">
                        <div id="report_diterima"></div>
                    </div>
                    <div class="gap"></div>
                    <div class="col-lg-12">
                        <div id="report_peserta"></div>
                    </div>
                    <div class="gap"></div>
                    <div class="col-lg-12">
                        <div id="report_pelatihan"></div>
                    </div>
                    <div class="gap"></div>

                </div>
            </section>

            <div class="notice">
                Tolong teliti dan dibaca baik - baik akan informasi data diatas, agar tidak kesalahan informasi Terimakasih !
            </div>
            <button class="btn kembali" onclick="goBack()">
                <i class="fa fa-chevron-left"></i>Kembali
            </button>
        </div>
        <script type="text/javascript">
            $(document).ready(function () {
                // Start Diterima
                var title_diterima = 'Data Grafik Peserta Pelatihan Diterima Kerja';
                var y_title_diterima = 'Total Diterima';
                var categories_diterima = <?php echo json_encode($tahunDiterima) ?>;
                var data_diterima = <?php echo json_encode($dataDiterima) ?>;

                setChart('report_diterima', title_diterima, y_title_diterima, categories_diterima, data_diterima);
                // End of Diterima

                // Start Peserta
                var title_peserta = 'Data Grafik Jumlah Peserta Pelatihan';
                var y_title_peserta = 'Total Peserta';
                var categories_peserta = <?php echo json_encode($tahunPeserta) ?>;
                var data_peserta = <?php echo json_encode($dataPeserta) ?>;

                setChart('report_peserta', title_peserta, y_title_peserta, categories_peserta, data_peserta);
                // End of Peserta

                // Start Diterima
                var title_pelatihan = 'Data Grafik Jumlah Pelatihan';
                var y_title_pelatihan = 'Total Pelatihan';
                var categories_pelatihan = <?php echo json_encode($tahunPelatihan) ?>;
                var data_pelatihan = <?php echo json_encode($dataPelatihan) ?>;

                setChart('report_pelatihan', title_pelatihan, y_title_pelatihan, categories_pelatihan, data_pelatihan);

                // //Sarana Prasarana
                // var title_sarana_prasarana = 'Data Grafik Jumlah Sarana dan Prasarana';
                // var y_title_sarana_prasarana = 'Total Sarana dan Prasarana';
                // var categories_jenis = <?php echo json_encode($jenis) ?>;
                // var data_series_sarana_prasarana = <?php echo json_encode($dataSaranaPrasarana) ?>;

                // setChart('report_sarana_prasarana', title_sarana_prasarana, y_title_sarana_prasarana, categories_jenis, data_series_sarana_prasarana);
            });

            function setChart(classaName, mainTitle, yTitle, categories, dataSeries) {
//                console.log(dataSeries);
                var chart = {
                    type: 'column'
                };
                var title = {
                    text: mainTitle
                };
                var xAxis = {
                    categories: categories,
                    crosshair: true
                };
                var yAxis = {
                    min: 0,
                    title: {
                        text: yTitle
                    }
                };
                var tooltip = {
                    shared: true,
                    useHTML: true
                };
                var plotOptions = {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                };
                var credits = {
                    enabled: false
                };
                var series = dataSeries;

                var json = {};
                json.chart = chart;
                json.title = title;
                json.tooltip = tooltip;
                json.xAxis = xAxis;
                json.yAxis = yAxis;
                json.series = series;
                json.plotOptions = plotOptions;
                json.credits = credits;
                $('#' + classaName).highcharts(json);
            }
        </script> 
    </body>
</html>	