<?php
$kota = $_GET['name'];
$url = "http://localhost/Api_Disnaker/WebService/getDataBlkCity/" . $kota;
$client = curl_init($url);
curl_setopt($client, CURLOPT_RETURNTRANSFER, 1);
$response = curl_exec($client);
$result = json_decode($response, true);

?>

<html>
    <head>
        <link rel="stylesheet" href="assets/gaya.css" />
        <link rel='stylesheet' href='assets/css/font-awesome.css'>

        <script src="assets/jquery-3.3.1.js"></script>
        <link rel="stylesheet" href="assets/popup/jquery.fancybox.css" />
        <link rel="stylesheet" href="assets/gaya.css" />
        <script src="assets/popup/jquery.fancybox.js"></script>

        <script src = "assets/highchart/highcharts.js"></script>
        <script src=" assets/highchart/modules/exporting.js"></script>
        <script src="assets/highchart/modules/offline-exporting.js"></script>

        <script>
            function goBack() {
                window.history.back();
            }
        </script>

        <style type="text/css">
            .bagan{width: 100%; }
            h3{
                margin: 0px !important;
                font-family: 'Poppins', sans-serif;
                color: #000;
            }

            h3 i{
                font-size: 22.5px;
                margin-right: 3px;
                color: #d32f2f;
            }

            h3 span{
                color: #d32f2f;
                font-weight: bold;
            }

            p{
                margin: 4px 0px 4px 0px !important;
                font-family: 'Raleway', sans-serif;
                font-size: 13.5px !important;
                color: #212121;
            }

            .gap {margin-top:20px;}

            table > tbody > tr > td{
                padding: 4px 15px 4px 0px !important;
                border-top: transparent;
                font-family: 'Raleway', sans-serif;
                vertical-align: top;
                font-size: 13.5px !important;
                color: #212121;
            }

            .donlod{color: #388E3C !important;}

            .donlod:hover, .donlod:focus, .donlod:active{color: #2E7D32 !important;}

            .popupBody{margin: 10px !important;}

            .title{
                color: #6D4C41;
                font-weight: bold;
            }

            .kembali{
                border-radius: 2px !important;
                padding: 10px 16.5px !important;
                font-family: 'Raleway', sans-serif !important;
                font-size: 13px;
                margin: 15px 0px 12px 0px;
                background-color: #424242 !important;
                color: #fff !important;
                border:transparent;
                position: absolute;
            }

            .kembali:hover,
            .kembali:focus,
            .kembali:active{background-color: #212121 !important;}

            .kembali i{
                margin-right: 10px;
                font-size: 11.5px;
            }

            .notice{
                font-weight: bold;
                margin-top: 15px;
                font-family: 'Raleway', sans-serif;
                font-size: 13.5px !important;
                color: #d32f2f;
            }
        </style>
    </head>
    <body>
        <div class="bagan">
            <header class="popupHeader">
                <h3><i class="fa fa-copy"></i>  Data <span>UPT BLK <?php echo ucfirst($kota); ?></span></h3>
                <p>Berikut ini adalah data detail dari UPT BLK <?php echo ucfirst($kota); ?> </p>
            </header>
            <section class="popupBody">
                <table>              
                    <tbody>


                        <?php
                        if (!empty($result['datail_blk'])) {

                            foreach ($result['datail_blk'] as $data) {

                                ?>

                                <tr>
                                    <td class="title">Nama BLK</td>
                                    <td>:</td>
                                    <td>
                                        <?php echo $data['name']; ?>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="title">Alamat</td>
                                    <td>:</td>
                                    <td>
                                        <?php echo $data['alamat_lengkap']; ?>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="title">Dasar Hukum</td>
                                    <td>:</td>
                                    <td>
                                        <?php echo $data['dasar_hukum']; ?>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="title">Email</td>
                                    <td>:</td>
                                    <td>
                                        <?php echo $data['email']; ?>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="title">Level Akreditasi</td>
                                    <td>:</td>
                                    <td>
                                        <?php echo $data['level_akreditasi']; ?>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="title">Telpon / Fax</td>
                                    <td>:</td>
                                    <td>
                                        <?php echo $data['telepon']; ?> / <?php echo $data['fax']; ?>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="title">Nama Pimpinan</td>
                                    <td>:</td>
                                    <td>
                                        <?php echo $data['nama_pimpinan']; ?>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="title">Potensi Daerah</td>
                                    <td>:</td>
                                    <td>
                                        <?php echo $data['potensi_daerah']; ?>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="title">Kejuruan Unggulan</td>
                                    <td>:</td>
                                    <td>
                                        <?php echo $data['kejuruan_unggulan']; ?>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="title">Luas Area</td>
                                    <td>:</td>
                                    <td>
                                        <?php echo $data['luas_area']; ?> Ha
                                    </td>
                                </tr>

                                <tr>
                                    <td class="title">Jumlah Pegawai Fungsional</td>
                                    <td>:</td>
                                    <td>
                                        <?php echo $result['total_pegawai_fungsional']; ?> Orang
                                    </td>
                                </tr>

                                <tr>
                                    <td class="title">Jumlah Pegawai Staff</td>
                                    <td>:</td>
                                    <td>
                                        <?php echo $result['total_pegawai_staff']; ?> Orang
                                    </td>
                                </tr>

                                <tr>
                                    <td class="title">Jumlah Pegawai Struktural</td>
                                    <td>:</td>
                                    <td>
                                        <?php echo $result['total_pegawai_struktural']; ?> Orang
                                    </td>
                                </tr>

                                <?php
                                if (!empty($result['datail_Jumlah_pelatihan'])) {
                                    foreach ($result['datail_Jumlah_pelatihan'] as $key => $value) {

                                        ?>
                                        <tr>
                                            <td class="title">Jumlah Pelatihan <?php echo $value['tahun']; ?></td>
                                            <td>:</td>
                                            <td>
                                                <?php echo (float) $value['jml_peserta']; ?> Orang 
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="title">Jumlah Penempatan</td>
                                            <td>:</td>
                                            <td>
                                                <?php echo (float) $value['jml_penempatan']; ?> Orang
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }

                                ?>


                            </tbody>
                        </table> 

                        <?php
                    }
                } else {

                    ?>

                    <?php
                    echo "Data detail UPT BLK " . $kota . " masih kosong";
                }

                ?>

                <br><br>

                <a href="kabupaten.php?name=<?php echo $kota ?>"><button class='w3-btn w3-red'><i class="fa fa-bar-chart" aria-hidden="true"></i></i> Data Grafik</button></a>&nbsp;
                <a href="data_tabel.php?name=<?php echo $kota ?>"><button class='w3-btn w3-red'><i class='fa fa-database'></i> Data Tabel</button></a>&nbsp;
                <a href="detail_apbn_apbd_swadana.php?name=<?php echo $kota ?>&sumber_dana=APBN"><button class='w3-btn w3-red'><i class='fa fa-database'></i> Pelatihan APBN</button></a>&nbsp;
                <a href="detail_apbn_apbd_swadana.php?name=<?php echo $kota ?>&sumber_dana=APBD"><button class='w3-btn w3-red'><i class='fa fa-database'></i> Pelatihan APBD</button></a>&nbsp;
                <a href="detail_apbn_apbd_swadana.php?name=<?php echo $kota ?>&sumber_dana=Swadana"><button class='w3-btn w3-red'><i class='fa fa-database'></i> Pelatihan Swadana</button></a>&nbsp;
                <a href="detail_mitra_kerja.php?name=<?php echo $kota ?>"><button class='w3-btn w3-red'><i class='fa fa-database'></i> Mitra Kerja</button></a>


            </section>

            <div class="notice">
                Tolong teliti dan dibaca baik - baik akan informasi data diatas, agar tidak kesalahan informasi Terimakasih !
            </div>
            <button class="btn kembali" onclick="goBack()">
                <i class="fa fa-chevron-left"></i>Kembali
            </button>
        </div>

    </body>
</html>